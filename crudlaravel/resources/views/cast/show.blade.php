@extends('adminlte.master')

@section('title')
Detail Pemain Film
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<h5>{{$cast->umur}} tahun</h5>
<p>{{$cast->bio}}</p>

@endsection