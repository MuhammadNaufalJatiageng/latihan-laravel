@extends('adminlte.master')

@section('title')
List Pemain Film
@endsection

@section('content')
@auth
  <a href="/cast/create" class="btn btn-secondary mb-3">Tambah Pemain</a>
@endauth

<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item -> nama}}</td>
            <td>{{$item -> umur}}</td>
            <td>{{$item -> bio}}</td>
            <td>
              @auth
                <form action="/cast/{{$item->id}}" method="POST">
                  <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  @csrf
                  @method('delete')
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
              @endauth
              @guest
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              @endguest
            </td>
          </tr>
      @empty
          <h1>Data Tidak Ada!</h1>
      @endforelse
    </tbody>
  </table>

@endsection