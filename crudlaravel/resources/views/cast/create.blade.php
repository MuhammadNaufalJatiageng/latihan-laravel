@extends('adminlte.master')

@section('title')
Tambah Pemain Film
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Pemain Film</label>
      <input type="text" class="form-control" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" class="form-control" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea  class="form-control" name="bio"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection