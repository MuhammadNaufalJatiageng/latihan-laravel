@extends('adminlte.master')

@section('title')
Tambah Genre Film
@endsection

@section('content')
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Genre Film</label>
      <input type="text" class="form-control" name="nama">
    </div>
    @error('genre')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection