@extends('adminlte.master')

@section('title')
Daftar Genre
@endsection

@section('content')
@auth
    <a href="/genre/create" class="btn btn-secondary btn-sm mb-3">Tambah Genre</a>
@endauth

<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($genre as $key => $item)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item -> nama}}</td>
            <td> 
              <form action="/genre/{{$item->id}}" method="POST">
                <a href="/genre/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                @auth
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
                @endauth
            </td>
          </tr>
      @empty
          <h1>Data Tidak Ada!</h1>
      @endforelse
    </tbody>
  </table>

@endsection