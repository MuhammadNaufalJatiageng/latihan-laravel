@extends('adminlte.master')

@section('title')
Ubah Profile
@endsection

@section('content')
<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
    </div>  
    
    <div class="form-group">
      <label>Umur</label>
      <input type="number" class="form-control" value="{{$profile->umur}}" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea  class="form-control" name="bio">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>alamat</label>
      <textarea  class="form-control" name="alamat">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection