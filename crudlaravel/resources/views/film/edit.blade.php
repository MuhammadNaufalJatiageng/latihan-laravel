@extends('adminlte.master')

@section('title')
Tambah Film
@endsection

@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" class="form-control" name="judul" value="{{$film->judul}}">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Ringkasan</label>
      <textarea  class="form-control" rows="10" name="ringkasan">{{$film->ringkasan}}</textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Tahun</label>
      <input type="number" class="form-control" name="tahun" value="{{$film->tahun}}">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="form-control">
          <option value="">---Pilih Genre---</option>
          @foreach ($genre as $item)
            @if ($item->id === $film->genre_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
          @endforeach
        </select>
    </div>
    @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Poster</label>
      <input type="file" class="form-control" name="poster" >
    </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection 