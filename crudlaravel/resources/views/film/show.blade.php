@extends('adminlte.master')
@section('title')
Detail Film
@endsection

@section('content')

<img style="display: block; margin-left: auto; margin-right: auto; margin-bottom: 5px;" src="{{asset('gambar/'. $film->poster)}}" alt="Gambarnya gatau kemana">
<h2>{{$film->judul}}</h2>
<h4>{{$film->tahun}}</h4>
<p>{{$film->ringkasan}}</p>

<h1>Komentar</h1>
@foreach ($film->kritik as $item)
  <hr>
  <h5 ><b>{{$item->user->name}}</b></h5>
  <h5><b><i>{{$item->point}} Point</i></b></h5>
  <p>{{$item->content}}</p>
@endforeach

<form action="/kritik" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Komentar</label>
      <input type="hidden" name="films_id" value="{{$film->id}}">
      <textarea  class="form-control" name="content" placeholder="Tulis Komentar Anda Disini!"></textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <select name="point" class="my-2">
      <option>---Berikan Rating---</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select><br>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection