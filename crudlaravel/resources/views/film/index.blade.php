@extends('adminlte.master')

@section('title')
Daftar Film
@endsection

@section('content')
@auth
  <a href="/film/create" class="btn btn-secondary mb-3">Tambah film</a>
  <a href="/genre/create" class="btn btn-secondary mb-3">Tambah Genre</a>
@endauth

<div class="row">
  @forelse ($film as $item)
    <div class="col-4">
      <div class="card" style="width: 18rem; height: 40rem;">
          <img class="card-img-top" style="height: 23rem;" src="{{asset('gambar/'. $item->poster)}}" alt="Card image cap">
          <div class="card-body">
            <span class="badge badge-info">{{$item->genre->nama}}</span>
            <h3>{{$item->judul}}</h3>
            <h5>{{$item->tahun}}</h6>
            <p class="card-text">{{Str::limit($item->ringkasan, 70, $end='...')}}</p>
            <form action="/film/{{$item->id}}" method="POST">
              @csrf
              @method('DELETE')
              <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
              @auth
              <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="delete" class="btn btn-danger btn-sm">
              @endauth
            </form>
          </div>
      </div>
    </div>
  @empty 
      <h1>Film Tidak Ditemukan</h1>
  @endforelse
</div>

@endsection