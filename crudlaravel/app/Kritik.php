<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    protected $table = 'kritik';
    protected $fillable = ['users_id', 'films_id', 'content', 'point'];

    public function film(){
        return $this->belongsTo('App\Film', 'films_id');
    }
    
    public function user(){
        return $this->belongsTo('App\User', 'users_id');
    }
}
