<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Profile;
use RealRashid\SweetAlert\Facades\Alert;


class ProfileController extends Controller
{
    public function index() {
        $profile = Profile::where('user_id', Auth::id())->first();
        
        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $profile_id){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required'
        ]);

        $profile = Profile::find($profile_id);

        $profile->umur = $request['umur'];
        $profile->bio = $request['bio'];
        $profile->alamat = $request['alamat'];

        $profile->save();

        Alert::success('Berhasil !', 'Profile berhasil disimpan.');


        return redirect('/profile');
    }
}
