<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use RealRashid\SweetAlert\Facades\Alert;

class GenreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function create(){
        return view('genre.create');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required',
        ]);
        
        $genre = new Genre;
        
        $genre->nama = $request->nama;
        
        $genre->save();

        Alert::success('Berhasil !', 'Anda berhasil menambahkan genre.');
        
        return redirect('/genre');
    }
    
    public function index(){
        $genre = Genre::all();
        return view('genre.index', compact('genre'));
    }

    public function show($id) {
        $genre = Genre::where('id', $id)->first();
        return view('genre.show', compact('genre'));
    }

    public function destroy($genre_id){

        $genre = Genre::find($genre_id);

        $genre->delete();

        Alert::success('Berhasil !', 'Anda berhasil menghapus genre '. ($genre->nama));


        return redirect('/genre');
    }
}
