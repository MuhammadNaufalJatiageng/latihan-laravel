<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Kritik;

class KritikController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'content' => 'required',
            'point' => 'required'
        ]);

        $kritik = new Kritik;

        $kritik->content = $request->content;
        $kritik->users_id = Auth::id();
        $kritik->films_id = $request->films_id;
        $kritik->point = $request->point;

        $kritik->save();

        return redirect()->back();
    }
}
