<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use App\Genre;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function create() {
        $genre = Genre::all();
        return view('film.create', compact('genre'));
    }

    public function store(Request $request) {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ]);

        $posterName = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('gambar'), $posterName);

        $film = new Film;
 
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $posterName;
        $film->genre_id = $request->genre_id;
 
        $film->save();
        
        Alert::success('Berhasil !', 'Anda berhasil menambahkan Film.');
        
        return redirect('/film');
    }

    public function index() {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    public function show($film_id) {
        $film = Film::where('id', $film_id)->first();
        return view('film.show', compact('film'));
    }

    public function edit($id) {
        $genre = Genre::all();
        $film = Film::where('id', $id)->first();
        Alert::success('Berhasil !', 'Anda berhasil mengubah Film.');
        return view('film.edit', compact('genre', 'film'));

    }

    public function update(Request $request, $id) {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ]);


        $film = Film::find($id);

        if ($request->has('poster')) {
           $posterName = time().'.'.$request->poster->extension();
           $request->poster->move(public_path('gambar'), $posterName);
    
           $film->judul = $request->judul;
           $film->ringkasan = $request->ringkasan;
           $film->tahun = $request->tahun;
           $film->poster = $posterName;
           $film->genre_id = $request->genre_id;
        } else {
            $film->judul = $request->judul;
           $film->ringkasan = $request->ringkasan;
           $film->tahun = $request->tahun;
           $film->genre_id = $request->genre_id;
        }
        

        $film->update();

        return redirect('/film');
        
    }

    public function destroy($id){

        $film = Film::find($id);
        $path = "gambar/";
        File::delete($path. $film->poster);
        $film->delete();

        Alert::success('Berhasil !', 'Anda berhasil menghapus film '. ($film->judul));


        return redirect('/film');
    }
}