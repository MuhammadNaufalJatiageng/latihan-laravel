<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'films';
    protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster', 'genre_id'];

    public function genre(){
        return $this->belongsTo('App\Genre');
    }

    public function kritik(){
        return $this->hasMany('App\Kritik', 'films_id');
    }
}
