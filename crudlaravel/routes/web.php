<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Indexcontroller@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@kirim');
Route::get('/master', function() {
    return view('adminlte.master');
});
Route::get('/items', function(){
    return view('items.index');
});
Route::get('/data-table', 'IndexController@table');

Route::get('/cast/create', 'CastController@create'); // Insert data CREATE
Route::get('/cast', 'CastController@index');    // Menampilkan data READ
Route::get('/cast/{cast_id}', 'CastController@show');   // Menampilkan detail dengan id READ
Route::post('/cast', 'CastController@store');   // Save data CREATE
Route::get('/cast/{cast_id}/edit', 'CastController@edit');   // UPDATE data  
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy'); // DELETE data
    
Route::resource('profile', 'ProfileController')->only(['index', 'update']);
Route::resource('genre', 'GenreController');
Route::resource('kritik', 'KritikController')->only(['index', 'store']);
Route::resource('film', 'FilmController');
Auth::routes();
